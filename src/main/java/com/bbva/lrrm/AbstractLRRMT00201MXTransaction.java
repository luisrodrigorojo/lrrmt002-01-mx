package com.bbva.lrrm;

import com.bbva.elara.transaction.AbstractTransaction;

/**
 * In this class, the input and output data is defined automatically through the setters and getters.
 */
public abstract class AbstractLRRMT00201MXTransaction extends AbstractTransaction {

	public AbstractLRRMT00201MXTransaction(){
	}


	/**
	 * Return value for input parameter nuCuenta
	 */
	protected String getNucuenta(){
		return (String)this.getParameter("nuCuenta");
	}

	/**
	 * Return value for input parameter cdCuenta
	 */
	protected String getCdcuenta(){
		return (String)this.getParameter("cdCuenta");
	}

	/**
	 * Return value for input parameter cdDivisa
	 */
	protected String getCddivisa(){
		return (String)this.getParameter("cdDivisa");
	}

	/**
	 * Set value for String output parameter importe
	 */
	protected void setImporte(final String field){
		this.addParameter("importe", field);
	}
}
